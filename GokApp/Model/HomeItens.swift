//
//  HomeItens.swift
//  GokApp
//
//  Created by Yuri Chaves on 26/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation

class HomeItens: Codable {
    var spotlight: [Spotlight]?
    var products: [Product]?
    var cash: CashItem?
}


struct Spotlight: Codable {
    let name: String?
    let bannerUrl: String?
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case bannerUrl = "bannerURL"
        case name
        case description
    }
}

struct Product: Codable{
    let name: String?
    let imageUrl: String?
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case imageUrl = "imageURL"
        case name
        case description
    }
}

struct CashItem: Codable {
    let title: String?
    let bannerUrl: String?
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case bannerUrl = "bannerURL"
        case title
        case description
    }
}
