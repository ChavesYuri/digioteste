//
//  CollectionView+Extension.swift
//  GokApp
//
//  Created by Yuri Chaves on 27/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    
    func setLayout(scrollDirection: ScrollDirection, itemSize: CGSize, minimumLineSpace: CGFloat, minimumInteritemSpacing: CGFloat, sectionInset: UIEdgeInsets){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = scrollDirection
        layout.itemSize = itemSize
        layout.minimumLineSpacing = minimumLineSpace
        layout.minimumInteritemSpacing = minimumInteritemSpacing
        layout.sectionInset = sectionInset
        
        self.collectionViewLayout = layout
    }
    
}
