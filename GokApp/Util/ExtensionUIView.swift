//
//  ExtensionUIView.swift
//  GokApp
//
//  Created by Yuri Chaves on 26/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func setBackground(cornerRadius: CGFloat, borderColor: UIColor?, borderWidth: CGFloat, backgroundColor: UIColor?,
        shadowColor: UIColor, shadowOffset: CGSize, shadowOpacity: Float ){
        self.layer.cornerRadius =  cornerRadius
        self.layer.borderColor = borderColor != nil ? borderColor!.cgColor : self.backgroundColor!.cgColor
        self.layer.borderWidth = borderWidth
        self.backgroundColor = backgroundColor != nil ? backgroundColor : self.backgroundColor
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = shadowOpacity
    }
}
