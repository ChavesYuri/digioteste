//
//  ProductsCell.swift
//  GokApp
//
//  Created by Yuri Chaves on 27/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import UIKit

protocol ProductViewModelProtocol: CellViewModelProtocol {
    var url: String { get }
}

class ProductsCell: UICollectionViewCell, CellProtocol {
    var viewModel: CellViewModelProtocol?
    
    func didLoad() {
        updateUI()
    }
    
    func didTap() {
        viewModel?.didTap()
    }
    
    override func draw(_ rect: CGRect) {
        self.imageViewBackground.clipsToBounds = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 8
        imageViewBackground.setBackground(cornerRadius: 8, borderColor: UIColor.darkGray, borderWidth: 0, backgroundColor: UIColor(white: 0.98, alpha: 1), shadowColor: UIColor(white: 0.90, alpha: 1), shadowOffset: CGSize(width: 0.5, height: 0.5), shadowOpacity: 0.7)
    }

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var imageViewBackground: UIView!
    
   
    func updateUI(){
        guard let viewModel = viewModel as? ProductViewModelProtocol else { return }
        
        if let url = URL(string: viewModel.url){
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: url, placeholder: .none, options: [.transition(.fade(0.7))], progressBlock: .none) { (result) in
                switch result{
                case .success(_ ):
                    break
                case .failure(let error ):
                    print(error.localizedDescription)
                    break
                }
            }
        }
    }


}
