//
//  ProductCellViewModel.swift
//  GokApp
//
//  Created by Yuri Chaves on 27/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation

class ProductCellViewModel: ProductViewModelProtocol{
    
    init(url: String) {
        self.url = url
    }
    
    var url: String
    
    var identifier: String = String(describing: ProductsCell.self)
    
    var cell: CellProtocol?
    
    var parentViewModel: CellListViewModelProtocol?
    
    func didTap() {
        // Action on cell
    }
    
    
}
