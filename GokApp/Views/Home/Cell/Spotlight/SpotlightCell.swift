//
//  SpotlightCell.swift
//  GokApp
//
//  Created by Yuri Chaves on 26/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import UIKit
import Kingfisher


protocol SpotilightViewModelProtocol: CellViewModelProtocol {
    var url: String { get }
}

class SpotlightCell: UICollectionViewCell, CellProtocol {
    var viewModel: CellViewModelProtocol?
    
    func didLoad() {
        updateUI()
    }
    
    func didTap() {
        viewModel?.didTap()
    }
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var backgroundImageView: UIView!
    
   override func draw(_ rect: CGRect) {
       self.backgroundImageView.clipsToBounds = false
       imageView.clipsToBounds = true
       imageView.contentMode = .scaleAspectFill
       imageView.layer.cornerRadius = 8
       backgroundImageView.setBackground(cornerRadius: 8, borderColor: UIColor.darkGray, borderWidth: 0, backgroundColor: UIColor(white: 0.98, alpha: 1), shadowColor: UIColor(white: 0.90, alpha: 1), shadowOffset: CGSize(width: 0.5, height: 0.5), shadowOpacity: 0.7)
   }
    
    
    private func updateUI(){
        guard let viewModel = viewModel as? SpotilightViewModelProtocol else { return }
        
        if let url = URL(string: viewModel.url){
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: url, placeholder: .none, options: [.transition(.fade(0.7))], progressBlock: .none) { (result) in
                switch result{
                case .success(_ ):
                    break
                case .failure(let error):
                    
                    print(error.localizedDescription)
                    break
                }
            }
        }
        
    }

}

