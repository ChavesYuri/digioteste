//
//  SpotlightCellViewModel.swift
//  GokApp
//
//  Created by Yuri Chaves on 26/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation
import UIKit

struct SpotilightCellViewModel: SpotilightViewModelProtocol {
    
    init(url: String) {
        self.url = url
    }
    
    var url: String
    
    var identifier: String = String(describing: SpotlightCell.self)
    
    var cell: CellProtocol?
    
    var parentViewModel: CellListViewModelProtocol?
    
    func didTap() {
       // Did tap
    }
    
    
    
}

