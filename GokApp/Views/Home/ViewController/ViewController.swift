//
//  ViewController.swift
//  GokApp
//
//  Created by Yuri Chaves on 26/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import UIKit
import Kingfisher

class ViewController: BaseViewController {
    
    //MARK: Labels Outlet
    @IBOutlet var uiLabels: [UILabel]!
    @IBOutlet var logoView: UIView!
    
    //MARK: CollectionView
    @IBOutlet var spotlightCollectionView: UICollectionView!
    @IBOutlet var productsCollectionView: UICollectionView!
    @IBOutlet var digioCashImageView: UIImageView!
    
    //Mark: ScrollView
    @IBOutlet var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        configureRefreshControl()
        guard let viewModel = viewModel as? HomeViewModel else { return }
        viewModel.fetchHomeData()
    }
    
    
    func setupCollectionView(){
        self.spotlightCollectionView.register(UINib(nibName: String(describing: SpotlightCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: SpotlightCell.self))
        
        self.spotlightCollectionView.setLayout(scrollDirection: .horizontal, itemSize: CGSize(width: (self.view.frame.width - 50), height: 148), minimumLineSpace: 20, minimumInteritemSpacing: 20, sectionInset: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15))
        
        self.productsCollectionView.setLayout(scrollDirection: .horizontal, itemSize: CGSize(width:  100, height: 100), minimumLineSpace: 10, minimumInteritemSpacing: 10, sectionInset: UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 18))
        
        self.productsCollectionView.register(UINib(nibName: String(describing: ProductsCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ProductsCell.self))
    }
    
    
    func hideLabels(hide: Bool){
        for x in self.uiLabels{
            x.isHidden = hide
        }
        self.logoView.isHidden = hide
    }
    
    func configureRefreshControl() {
       scrollView.refreshControl = UIRefreshControl()
       scrollView.refreshControl?.addTarget(self, action:
                                          #selector(handleRefreshControl),
                                          for: .valueChanged)
    }
    
    @objc func handleRefreshControl() {
        guard let viewModel = viewModel as? HomeViewModel else { return }
        DispatchQueue.main.async {
            self.scrollView.refreshControl?.beginRefreshing()
        }
        viewModel.fetchHomeData()
    }
    
    
    

}

// MARK: Data Source
extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewModel = viewModel as? HomeViewModel else {
            return 0
        }
        if collectionView == self.productsCollectionView{
            return viewModel.itemsProduct.count
        }else{
            return viewModel.itemsSpotlight.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let viewModel = viewModel as? HomeViewModel else {
            return UICollectionViewCell()
        }
        var cellViewModel = collectionView == self.spotlightCollectionView ? viewModel.itemsSpotlight[indexPath.row] :  viewModel.itemsProduct[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellViewModel.identifier, for: indexPath)
        
        if let cell = cell as? CellProtocol {
            cell.viewModel = cellViewModel
            cellViewModel.parentViewModel = viewModel
            cellViewModel.cell = cell
            cell.didLoad()
        }
        return cell
        
    }
    
    
}

// MARK: Display Events
extension ViewController: HomeViewModelDisplayProtocol{
    func displayComponents() {
        self.spotlightCollectionView.reloadData()
        self.productsCollectionView.reloadData()
        self.hideLabels(hide: false)
        self.scrollView.refreshControl?.endRefreshing()
    }
    
    func displayDigioCash(url: URL) {
        self.digioCashImageView.kf.indicatorType = .activity
        self.digioCashImageView.kf.setImage(with: url, placeholder: .none, options: [.transition(.fade(0.7))], progressBlock: .none) { (result) in
            switch result{
            case .success(_ ):
                break
            case .failure(let error):
                // TODO: Setar uma imagem com erro ou esconder a celula
                print(error)
            }
        }
    }
    
}





