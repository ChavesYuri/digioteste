//
//  HomeViewModel.swift
//  GokApp
//
//  Created by Yuri Chaves on 27/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation


class HomeViewModel:  HomeViewModelProtocol, CellListViewModelProtocol{
    var itemsSpotlight =  [CellViewModelProtocol]()
    
    var itemsProduct = [CellViewModelProtocol]()
    
    var viewModelDisplay: ViewModelDisplayProtocol?
    
    var tabbarItemText: String = ""
    
    var navTitleText: String = ""
    

    var response = HomeItens()
    
    func fetchHomeData() {
        guard let viewModelDisplay = viewModelDisplay as? ViewController else { return }
        viewModelDisplay.displayLoading(true)
        ServiceLayer.request(router: .getHome) { [weak self](result: Result<HomeItens, Error>) in
            guard let self = self else { return }
            switch result{
            case .success(let response):
                if response.cash != nil && response.products != nil && response.spotlight != nil {
                    self.response = response
                    self.itemsProduct.removeAll()
                    self.itemsSpotlight.removeAll()
                    self.makeItems()
                    if let url = URL(string: response.cash?.bannerUrl ?? ""){
                        viewModelDisplay.displayDigioCash(url: url)
                    }
                    viewModelDisplay.displayComponents()
                }else{
                    viewModelDisplay.displayError(nil)
                }
                break
            case .failure(let error):
                viewModelDisplay.displayError(error.localizedDescription)
                break
            }
            viewModelDisplay.displayLoading(false)
            viewModelDisplay.scrollView.refreshControl?.endRefreshing()
        }
    }
    
    
    
    func makeItems(){
        
        self.makeSpotilightItems()
        self.makeProductsItems()
        
    }
    
    func makeSpotilightItems(){
        guard let spotlights = self.response.spotlight else { return }
        let compactComponents = spotlights.compactMap{$0}
        let models = compactComponents.map{SpotilightCellViewModel(url: $0.bannerUrl ?? "")}
        self.itemsSpotlight.append(contentsOf: models)
    }
    
    func makeProductsItems(){
        guard let products = self.response.products else { return }
        let compactComponents = products.compactMap{$0}
        let models = compactComponents.map{ProductCellViewModel(url: $0.imageUrl ?? "")}
        self.itemsProduct.append(contentsOf: models)
    }
    
    
}
