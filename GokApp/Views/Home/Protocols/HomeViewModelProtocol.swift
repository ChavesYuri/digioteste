//
//  HomeViewModelProtocol.swift
//  GokApp
//
//  Created by Yuri Chaves on 27/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation

protocol HomeViewModelProtocol: ViewModelProtocol {
    func fetchHomeData()
}

protocol HomeViewModelDisplayProtocol: ViewModelDisplayProtocol {
    func displayComponents()
    func displayDigioCash(url: URL)
}
