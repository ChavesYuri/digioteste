//
//  BaseProtocols.swift
//  GokApp
//
//  Created by Yuri Chaves on 26/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation


// MARK: - Loading and Error
protocol ViewModelDisplayProtocol {
    func displayLoading(_ loading: Bool)
    func displayError(_ error: String?)
}

//MARK: - ViewModel

protocol ViewModelProtocol {
    var viewModelDisplay: ViewModelDisplayProtocol? { get set }
    var tabbarItemText: String { get }
    var navTitleText: String { get }
}


protocol ViewModelDrivenProtocol: class {
    var viewModel: ViewModelProtocol? { get }
}


//MARK: - Cell List
protocol CellListViewModelProtocol {
    var itemsSpotlight: [CellViewModelProtocol] { get }
    var itemsProduct: [CellViewModelProtocol] { get }
}

protocol CellViewModelProtocol {
    var identifier: String { get }
    var cell: CellProtocol? { get set }
    var parentViewModel: CellListViewModelProtocol? { get set }
    func didTap()
}

protocol CellProtocol: class {
    var viewModel: CellViewModelProtocol? { get set }
    func didTap()
    func didLoad()
}

extension CellProtocol {
    func didTap(){
        viewModel?.didTap()
    }
}

