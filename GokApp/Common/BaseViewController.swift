//
//  BaseViewController.swift
//  GokApp
//
//  Created by Yuri Chaves on 26/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController, ViewModelDrivenProtocol, ViewModelDisplayProtocol{
    
    var viewModel: ViewModelProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel?.tabbarItemText
        navigationItem.title = viewModel?.navTitleText
    }
}

extension BaseViewController {
    
    func displayLoading(_ loading: Bool){
        loading ? showProgressIndicator(view: self.view) : hideProgressIndicator(view: self.view)
    }
    
    open func displayError(_ error: String?){
        showAlertMessage(vc: self, titleStr: "Ops!", messageStr: error ?? "Desculpe, ocorreu um erro ao processar sua solicitação")
    }
    
}
