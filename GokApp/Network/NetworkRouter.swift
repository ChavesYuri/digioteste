//
//  NetworkRouter.swift
//  GokApp
//
//  Created by Yuri Chaves on 26/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import Foundation
enum NetworkRouter{
    
    case getHome
    
    var scheme: String {
        switch self {
        case .getHome:
            return "https"
        }
    }
    
    var host: String {
        switch self {
        case .getHome:
            return "7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com"
        }
    }
    
    var path: String {
        switch self {
        case .getHome:
            return "/sandbox/products"
        }
    }
    
    var parameters: [URLQueryItem] {
        switch self {
        case .getHome:
            return []
        }
    }
    
    var method: String {
      switch self {
        case .getHome:
          return "GET"
      }
    }
}
