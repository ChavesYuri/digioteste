//
//  GokAppTests.swift
//  GokAppTests
//
//  Created by Yuri Chaves on 26/10/20.
//  Copyright © 2020 Yuri Chaves. All rights reserved.
//

import XCTest
@testable import GokApp

class GokAppTests: XCTestCase {
    
    override func setUpWithError() throws {
        super.setUp()
    }
    
    override func tearDownWithError() throws {
        super.tearDown()
    }
    
    func testRequestAsync() throws {
        let promise = expectation(description: "No error in response")
        var responseError: Error?
        var sut: HomeItens?
        
        ServiceLayer.request(router: NetworkRouter.getHome) { (result: Result< HomeItens, Error>) in
            
            switch result{
            case .success(let itens):
                sut = itens
                break
            case .failure(let error):
                responseError = error
                break
            }
            promise.fulfill()
        }
        
        wait(for: [promise], timeout: 5)
        
        XCTAssertNil(responseError)
        XCTAssertNotNil(sut)
    }
    
    func testRequestMock() throws{
        guard
            let path = Bundle(for: type(of: self)).path(forResource: "home", ofType: "json")
            else { fatalError("Can't find home.json file") }
        
        let data = try Data(contentsOf: URL(fileURLWithPath: path))
        let response = try JSONDecoder().decode(HomeItens.self, from: data)
        XCTAssertNotNil(response)
        XCTAssertNotNil(response.products)
        XCTAssertEqual(3, response.products?.count)
        XCTAssertNotNil(response.spotlight)
        XCTAssertEqual(2, response.spotlight?.count)
        XCTAssertNotNil(response.cash)
        
        
    }
    
    func testPerformanceExample() throws {
        self.measure {
            
        }
    }
    
}
